FROM node:12
# Create index directory
WORKDIR /index
ADD . /index
# Install index dependencies
RUN npm install
EXPOSE 8080
CMD [ "node", "index.js" ]